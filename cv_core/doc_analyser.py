import json
import os

import concurrent.futures as cf
from concurrent.futures import ThreadPoolExecutor as PoolExecutor

import cv2.cv2
import fitz
import numpy as np
from PIL import Image

from cv_core import cv_logic


class DocumentType(object):

    def __init__(self, title: str, key_phrases: list, check_stamps: bool, check_company_name: bool, check_inn: bool):
        self.title = title
        self.do_stamps_check = check_stamps
        self.do_company_name_check = check_company_name
        self.do_inn_check = check_inn
        self.key_phrases = key_phrases

    def check_document_type(self, text_pages) -> bool:
        # print('Document type', self.title)
        title_search_result = False
        for page in text_pages:
            # print('Searching title on', page)
            for variant in page:
                title_search_result |= cv_logic.find_in_text(variant, self.title, lowercase=True)
        if not title_search_result:
            return False
        key_phrases_search_result = len(self.key_phrases) == 0
        for page in text_pages:
            for phrase in self.key_phrases:
                # print('Searching phrase', phrase, 'on', page)
                for variant in page:
                    key_phrases_search_result |= cv_logic.find_in_text(variant, phrase, lowercase=True)
        return title_search_result and key_phrases_search_result

    def check_company_name(self, text_pages, real_company_name) -> bool:
        result = False
        for page in text_pages:
            for variant in page:
                result |= cv_logic.find_in_text(variant, real_company_name, lowercase=True)
        return result

    def check_inn(self, text_pages, real_inn) -> bool:
        result = False
        for page in text_pages:
            for variant in page:
                result |= cv_logic.find_in_text(variant, real_inn, threshold=0.8)
        return result

    def check_stamps(self, images) -> bool:
        result = False
        for image in images:
            result |= cv_logic.count_stamps(image) > 0
        return result


document_types = []


# document_types saving...
# with open(file="../document_types.json", mode="w", encoding="utf-8") as file:
#     document_types = [x.__dict__ for x in document_types]
#     file.write(json.dumps(document_types, indent=4))


def load_document_types():
    global document_types
    with open(file=os.path.dirname(__file__) + "/../document_types.json", mode="r", encoding="utf-8") as file:
        text = json.load(file)
        for d in text:
            document_types.append(DocumentType(d['title'], d['key_phrases'],
                                               d['do_stamps_check'], d['do_company_name_check'], d['do_inn_check']))
    print('Загружено типов документов:', len(document_types))
    print([x.title for x in document_types])


def __get_images_from_pdf__(path: str):
    images = []
    pdf_file = fitz.Document(path)
    for page_index in range(pdf_file.pageCount):
        img = pdf_file.getPageImageList(page_index)[0]  # pick the first one, others is small variants for os
        xref = img[0]
        base_image = pdf_file.extractImage(xref)
        image_bytes = base_image['image']
        # image_ext = base_image['ext']
        image = cv2.imdecode(np.frombuffer(image_bytes, dtype=np.uint8), cv2.IMREAD_COLOR)
        images.append(image)
    pdf_file.close()
    return images


def convert_file_to_images(path: str):
    ext = path.rsplit(".", maxsplit=1)
    ext = ext[-1].rsplit("_", maxsplit=1)[0]
    if ext == 'pdf':
        return __get_images_from_pdf__(path)
    if ext in ['jpg', 'png', 'jpeg', 'tif', 'tiff', 'bmp']:
        # Здесь лучше использовать cv2.imread, но он работает с багами на windows поэтому костыль через PIL
        return [np.asarray(Image.open(path))]


# Асинхронность на все ядра
def __read_document_images__(images):
    result = [[] for x in images]
    with PoolExecutor() as executor:
        for i in range(len(result)):
            for filtr in cv_logic.filters:
                future = executor.submit(cv_logic.extract_text_from_page, i, images[i], 'rus', filtr, '--oem 3 --psm 6')
                def when_finished(f: cf.Future):
                    idx, text = f.result()
                    result[idx].append(text)
                future.add_done_callback(when_finished)
        executor.shutdown(wait=True)
    return result


# Основная логическая функция прототипа
def classify_document(filepath: str, company_name: str, inn: str):
    images = convert_file_to_images(filepath)
    text_pages = __read_document_images__(images)
    for doc_type in document_types:
        if doc_type.check_document_type(text_pages):
            if doc_type.do_inn_check:
                if not doc_type.check_inn(text_pages, inn):
                    return {'result': 'recognition_error', 'doc_type': doc_type.title, 'message': 'Несоответствие ИНН'}
            if doc_type.do_company_name_check:
                if not doc_type.check_company_name(text_pages, company_name):
                    return {'result': 'recognition_error', 'doc_type': doc_type.title,
                            'message': 'Несоответствие названия компании'}
            if doc_type.do_stamps_check:
                if not doc_type.check_stamps(images):
                    return {'result': 'recognition_error', 'doc_type': doc_type.title, 'message': 'Печати не найдены'}
            return {'result': 'ok', 'doc_type': doc_type.title}
    return {'result': 'recognition_error', 'doc_type': 'undefined', 'message': 'Документ не был распознан'}
