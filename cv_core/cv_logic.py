import re

import cv2
import numpy as np
import pytesseract

filters = (0, 1, 2)


def extract_text_from_page(i, nd_array: np.ndarray, language="rus", filter=0, config='--oem 3 --psm 6'):
    gray = cv2.cvtColor(nd_array, cv2.COLOR_BGR2GRAY)
    if filter == 0:
        gray = cv2.medianBlur(gray, 1)
    if filter == 1:
        gray = cv2.medianBlur(gray, 3)
        _, gray = cv2.threshold(gray, 127, 255, type=cv2.THRESH_BINARY)
    if filter == 2:
        gray = cv2.medianBlur(gray, 1)
        config = '--oem 3 --psm 3'
    text = pytesseract.image_to_string(image=gray, lang=language, config=config)
    return (i, text)


def count_stamps(image: np.ndarray):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.medianBlur(gray, 5)
    circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=0.7, minDist=30,
                               param1=85, param2=80, minRadius=100, maxRadius=200)
    if circles is None:
        return 0
    return circles.shape[1]


def find_in_text(text: str, query: str, lowercase=False, threshold=0.8, mode=0) -> bool:
    if lowercase:
        query = query.lower()
        text = text.lower()
    query = query.replace(' ', '').replace('\n', '')
    cleared = re.sub("[^" + query + "]", '', text) if mode == 0 else text.replace(' ', '').replace('\n', '')
    current = 0
    while current < len(cleared) - len(query):
        fragment = cleared[current:current + len(query)]
        ratio = __levenshtein_distance__(fragment, query)
        if ratio > threshold:
            return True
        current += 1
    return False


def __levenshtein_distance__(s, t):
    # Initialize matrix of zeros
    rows = len(s) + 1
    cols = len(t) + 1
    distance = np.zeros((rows, cols), dtype=int)
    # Populate matrix of zeros with the indeces of each character of both strings
    for i in range(1, rows):
        for k in range(1, cols):
            distance[i][0] = i
            distance[0][k] = k
    # Iterate over the matrix to compute the cost of deletions,insertions and/or substitutions
    for col in range(1, cols):
        for row in range(1, rows):
            if s[row - 1] == t[col - 1]:
                cost = 0  # If the characters are the same in the two strings in a given position [i,j] then the cost is 0
            else:
                # In order to align the results with those of the Python Levenshtein package, if we choose to calculate the ratio
                # the cost of a substitution is 2. If we calculate just distance, then the cost of a substitution is 1.
                cost = 2
            distance[row][col] = min(distance[row - 1][col] + 1,  # Cost of deletions
                                     distance[row][col - 1] + 1,  # Cost of insertions
                                     distance[row - 1][col - 1] + cost)  # Cost of substitutions
    Ratio = ((len(s) + len(t)) - distance[row][col]) / (len(s) + len(t))
    return Ratio
