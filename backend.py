import os
import re
import secrets
import tempfile
import uuid

from flask import Flask, request, jsonify, make_response

import cv_core.doc_analyser as analyser

app = Flask(__name__, static_url_path='/static/', static_folder='./static')
app.config["SECRET_KEY"] = secrets.token_urlsafe(16)


@app.route('/')
def submission_form():
    return app.send_static_file('forms/submission.html')


@app.route('/settings')
def administration_form():
    return app.send_static_file('forms/settings.html')


@app.route('/errors')
def error_form():
    return app.send_static_file('forms/errors.html')


@app.route('/check', methods=['POST'])
def check_document():
    inn, name, file = request.form.get('inn'), request.form.get('name'), request.files.get('document')
    if file and not __validate_uploads__(inn, name, file.filename):
        return make_response(jsonify({'result': 'error', 'message': 'Неправильная форма'}), 400)
    filepath = os.path.join(tempfile.gettempdir(), file.filename + '_' + uuid.uuid4().hex[0:5])
    file.save(filepath)
    response = analyser.classify_document(filepath, name, inn)
    os.remove(filepath)
    return make_response(jsonify(response), 200)


def __validate_uploads__(inn, name, file_name) -> bool:
    return re.match(pattern=r'^\d{10,12}$', string=inn) \
           and re.match(pattern=r'^.{1,}[.]((pdf)|(jpg)|(png)|(jpeg)|(tif)|(tiff)|(bmp))$', string=file_name) \
           and len(name) > 2


if __name__ == "__main__":
    analyser.load_document_types()
    app.run(port=8080, debug=False, threaded=True)
